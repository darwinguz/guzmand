//
//  ApiExamen.swift
//  GuzmanD
//
//  Created by Darwin Guzmán on 5/6/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import Foundation

struct ApiExamenInfo: Decodable {
    let viewTitle:String
    let date:String
    let nextLink:String
}

