//
//  ViewController.swift
//  GuzmanD
//
//  Created by Darwin Guzmán on 5/6/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let urlString = "https://api.myjson.com/bins/72936"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!){ (data, response, error) in
            guard let data = data else {
                print("Error NO data")
                return
            }
            guard let examenInfo = try? JSONDecoder().decode(ApiExamenInfo.self, from: data) else{
                print("Error decoding exam")
                return
            }
            //print(weatherInfo.weather[0].description)
            
            DispatchQueue.main.async {
                self.viewTitleLabel.text = "\(examenInfo.viewTitle)"
                self.dateLabel.text = "\(examenInfo.date)"
                //self.nameTextField.text = "\(examenInfo.apiExamenDatos[0].viewTitle)"
            
            }
            //self.resultLabel.text = "\"
        }
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

